import { makeStyles } from "@material-ui/core";
import { red, green } from "@material-ui/core/colors";

export default makeStyles(({ spacing }) => ({
  indicatorError: {
    color: red[500],
    marginRight: spacing(1),
  },
  indicatorSuccess: {
    color: green[500],
    marginRight: spacing(1),
  },
}));
