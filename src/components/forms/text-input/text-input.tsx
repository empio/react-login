import React from "react";
import { FieldRenderProps } from "react-final-form";
import { InputAdornment, TextField } from "@material-ui/core";
import { CheckCircle, Error } from "@material-ui/icons";
import useStyles from "./text-input.styles";

type Props = FieldRenderProps<HTMLElement> & {
  validationIndicator?: boolean;
};

const TextInput: React.FC<Props> = ({
  input: { name, onChange, value, ...restInput },
  meta,
  validationIndicator = false,
  ...rest
}) => {
  const classes = useStyles();

  let endAdornment;
  if (validationIndicator && meta.touched) {
    endAdornment = (
      <InputAdornment position="end">
        {(meta.submitError && !meta.dirtySinceLastSubmit) || meta.error ? (
          <Error className={classes.indicatorError} />
        ) : (
          <CheckCircle className={classes.indicatorSuccess} />
        )}
      </InputAdornment>
    );
  }

  return (
    <TextField
      {...rest}
      variant="outlined"
      margin="dense"
      fullWidth
      name={name}
      helperText={(meta.touched && meta.error) || " "}
      error={meta.error && meta.touched}
      InputProps={{
        inputProps: {
          autoComplete: "off",
          spellCheck: false,
          autoCorrect: "off",
          autoCapitalize: "none",
          ...restInput
        },
        endAdornment
      }}
      onChange={onChange}
      value={value}
    />
  );
}

export default TextInput;
