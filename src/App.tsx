import React, { useState } from 'react';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Login } from "./modules";

function App<React, FC>() {
  const [email, setEmail] = useState<string>("");
  const [password] = useState<string>("");

  function Notify(text: string) {
    toast(text, {
      autoClose: 3000
    });
  }

  return (
    <>
      <Login
        email={email}
        setEmail={setEmail}
        password={password}
        Notify={Notify}
      />
      <ToastContainer />
    </>
  );
}

export default App;
