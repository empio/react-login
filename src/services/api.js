const URL = "https://api.github.com";

export async function getUserAsync(name) {
  // try {
    const response = await fetch(`${URL}/users/${name}`);
    if (response.ok) return response.json();
    throw new Error(response.statusText);
  // } catch (err) {
  //   console.error("error", err)
  // }
}
