import validate from "validate.js";
import _ from "lodash";
import moment from "moment";

type FinalFormValues = {
  [key: string]: any;
};

validate.extend(validate.validators.datetime, {
  parse: (value: moment.MomentInput) => moment.utc(value),
  format: (value: moment.MomentInput) => moment.utc(value).format("L")
});

export const createValidator = (schema: object) => {
  const validator = (values: FinalFormValues) => {
    const errors = validate(values, schema);
    if (!errors) {
      return {};
    }

    return _.mapValues(errors, err =>
      Array.isArray(err) && err.length > 0 ? err[0] : err
    );
  };

  return validator;
};
