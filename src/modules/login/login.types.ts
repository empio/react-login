export interface Props {
  email: string;
  setEmail?: (email: string) => void;
  password: string;
  Notify?: (text: string) => void;
}
