import React from "react";
import { Field, Form } from "react-final-form";
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CircularProgress,
  Grid,
  Typography
} from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import { createValidator } from "../../utils/create-validator";
import { Props } from "./login.types";
import { getUserAsync } from "../../services/api";
import { TextInput } from "../../components";
import useStyles from "./login.styles";

const Login: React.FC<Props> = ({ email, setEmail, password, Notify }) => {
  const classes = useStyles();

  const [isPrivatePage, setPrivatePage] = React.useState<boolean>(false);

  const initialValues = {
    email: email,
    password: password
  }

  // const validate = (values: Props) => {
  //   const errors = {} as Props;
  //   const invalidEmail = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  //   if (!values.email) {
  //     errors.email = "Required";
  //   } else if(!values.email.match(invalidEmail)) {
  //     errors.email = "Invalid";
  //   }

  //   if (!values.password) {
  //     errors.password = "Required";
  //   } else if (values.password.length < 5) {
  //     errors.password = "Too short";
  //   }

  //   return errors;
  // }

  const validator = createValidator({
    // email: { presence: { allowEmpty: false } }
    // email: { presence: { allowEmpty: false }, email: { message: "is invalid" } }
    email: {
      presence: { allowEmpty: false },
      email: (_: any, attributes: any) =>
        attributes.email && {
          format: {
            pattern: /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
            message: "email is invalid"
          }
        }
    },
    password: { presence: { allowEmpty: false }, length: { minimum: 5 } }
  });

  // const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
  // const onSubmit = async values => {
  //   await sleep(300)
  //   console.log(JSON.stringify(values, 0, 2))
  // }

  const onSubmit = () => {
    return new Promise(resolve => {
      setTimeout(resolve, 2000);
    });
  }

  // import { useFormState } from "react-final-form-hooks"
  // export default function ResetOnSubmitSuccess({ form }) {
  //   const state = useFormState(form, { submitSucceeded: true })
  //   useEffect(() => {
  //     if (state.submitSucceeded) form.reset()
  //   }, [state.submitSucceeded])
  //   return null
  // }
  // function MyForm() {
  //   const { handleSubmit, form } = useForm({})
  //   return (
  //     <form onSubmit={handleSubmit}>
  //       <ResetOnSubmitSuccess form={form} />
  //       <button type="submit">Submit</button>
  //     </form>
  //   )
  // }

  const onLogout = () => {
    setPrivatePage(false);
    setEmail!("");
    Notify!("Success");
  }

  if (isPrivatePage) {
    return(
      <div className={classes.container}>
        <div>
          <Typography variant="h3">
            {email.split("@")[0]}
          </Typography>
          <Grid container justify="center">
            <Button
              className={classes.logoutBtn}
              variant="contained"
              color="primary"
              onClick={onLogout}
            >
              Logout
            </Button>
          </Grid>
        </div>
      </div>
    )
  }

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      // validate={values => validate(values)}
      validate={validator}
      render={({
        handleSubmit,
        submitting,
        form,
        pristine,
        hasValidationErrors,
        values
      })=>(
        <form
          className={classes.container}
          onSubmit={async event => {
            await handleSubmit(event);
            setEmail!(values.email);
            // form.reset()
            for (const key in values) {
              form.change(key, undefined);
              form.resetFieldState(key);
            }
            try {
              await getUserAsync(values.email.split("@")[0]);
              setPrivatePage(true);
              Notify!("Success");
            } catch (err) {
              Notify!("Not found");
            }
          }}
        >
          <Card className={classes.card}>
            <CardContent className={classes.header}>
              <Avatar className={classes.avatar}>
                <AccountCircle />
              </Avatar>
            </CardContent>
            <CardContent>
              <Field
                name="email"
                placeholder="E-mail"
                component={TextInput}
                validationIndicator
              />
              <Field
                name="password"
                placeholder="Password"
                type="password"
                component={TextInput}
                validationIndicator
              />
            </CardContent>
            <CardActions>
              <Button
                className={classes.loginBtn}
                variant="contained"
                color="secondary"
                type="submit"
                disabled={submitting || pristine || hasValidationErrors}
              >
                {submitting ?
                  <CircularProgress size={24} color="secondary" /> :
                  "Login"
                }
              </Button>
            </CardActions>
          </Card>
        </form>
      )}
    />
  );
}

export default Login;
