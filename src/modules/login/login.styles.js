import { makeStyles } from "@material-ui/core";
import { red, blueGrey } from "@material-ui/core/colors";

export default makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  card: {
    width: theme.spacing(40),
  },
  header: {
    background: blueGrey[900],
  },
  avatar: {
    backgroundColor: red[500],
    margin: "auto",
  },
  loginBtn: {
    flexGrow: 1,
  },
  logoutBtn: {
    marginTop: theme.spacing(2),
  },
}));
